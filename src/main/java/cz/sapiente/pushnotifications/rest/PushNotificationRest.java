package cz.sapiente.pushnotifications.rest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.sapiente.pushnotifications.dto.PushNotificationDto;
import cz.sapiente.pushnotifications.dto.SubscriptionDto;
import cz.sapiente.pushnotifications.service.PushNotificationService;
import lombok.NonNull;

@RestController
@CrossOrigin
@RequestMapping("/api/push-notifications")
public class PushNotificationRest {

    private final PushNotificationService pushNotificationService;

    public PushNotificationRest(@NonNull final PushNotificationService pushNotificationService) {
        this.pushNotificationService = pushNotificationService;
    }

    @PostMapping
    public void sendNotification(@RequestBody final PushNotificationDto notification) {
        this.pushNotificationService.sendNotification(notification);
    }

    @PutMapping("/subscriptions")
    public void storeSubscription(@RequestBody final SubscriptionDto subscription) {
        this.pushNotificationService.storeSubscription(subscription);
    }

    @DeleteMapping("/subscriptions")
    public void deleteSubscription(@RequestBody final SubscriptionDto subscription) {
        this.pushNotificationService.deleteSubscription(subscription);
    }
}
