package cz.sapiente.pushnotifications.dto;

import lombok.Data;

@Data
public class SubscriptionDto {
    private String endpoint;
    private String expirationTime;
    private Keys keys;

    @Data
    public class Keys {
        private String p256dh;
        private String auth;
    }
}
