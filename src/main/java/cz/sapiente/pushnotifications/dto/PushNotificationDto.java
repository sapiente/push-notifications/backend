package cz.sapiente.pushnotifications.dto;

import java.time.OffsetDateTime;

import lombok.Data;

@Data
public class PushNotificationDto {
    private String title;
    private String body;
    private OffsetDateTime date;
}
