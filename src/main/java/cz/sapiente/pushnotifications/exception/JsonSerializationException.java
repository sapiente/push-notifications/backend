package cz.sapiente.pushnotifications.exception;

public class JsonSerializationException extends RuntimeException {

    public JsonSerializationException(final Throwable cause) {
        super(cause);
    }
}
