package cz.sapiente.pushnotifications.service;

import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.PushService;

import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import cz.sapiente.pushnotifications.dto.PushNotificationDto;
import cz.sapiente.pushnotifications.dto.SubscriptionDto;
import cz.sapiente.pushnotifications.exception.PushNotificationException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PushNotificationService {

    @Value("${vapid.public.key}")
    private String vapidPublicKey;
    @Value("${vapid.private.key}")
    private String vapidPrivateKey;

    private List<SubscriptionDto> subscriptions = Collections.synchronizedList(new ArrayList<>());
    private PushService pushService;
    private final JsonSerializer jsonSerializer;

    public PushNotificationService(@NonNull final JsonSerializer jsonSerializer) {
        this.jsonSerializer = jsonSerializer;
    }

    public void storeSubscription(@NonNull final SubscriptionDto subscription) {
        if (this.subscriptions.stream().map(SubscriptionDto::getEndpoint).noneMatch(subscription.getEndpoint()::equals)) {
            this.subscriptions.add(subscription);
        }
    }

    public void deleteSubscription(final SubscriptionDto subscription) {
        this.subscriptions.removeIf(subscription::equals);
    }

    public void sendNotification(@NonNull final PushNotificationDto pushNotification) {
        this.subscriptions = this.subscriptions
                .stream()
                .filter(subscription -> {
                    final var response = this.sendNotification(pushNotification, subscription);
                    final var statusCode = response.getStatusLine().getStatusCode();

                    if(statusCode != 201) {
                        log.error("Push notification service returned {} status code for subscription {}", statusCode, subscription);
                    }

                    return !(statusCode == 404 || statusCode == 410);
                })
                .collect(Collectors.toList());
    }

    private HttpResponse sendNotification(final PushNotificationDto pushNotification, final SubscriptionDto subscription) {
        try {
            final var notification = new Notification(
                    subscription.getEndpoint(),
                    subscription.getKeys().getP256dh(),
                    subscription.getKeys().getAuth(),
                    this.jsonSerializer.serialize(pushNotification)
            );

            return this.getPushService().send(notification);
        } catch (Exception e) {
            throw new PushNotificationException(e);
        }
    }

    private PushService getPushService() {
        if (this.pushService == null) {
            synchronized (this) {
                if (this.pushService == null) {
                    try {
                        this.pushService = new PushService(this.vapidPublicKey, this.vapidPrivateKey, null);
                    } catch (GeneralSecurityException e) {
                        throw new PushNotificationException(e);
                    }
                }
            }
        }

        return this.pushService;
    }
}
