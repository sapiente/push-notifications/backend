# Requirements

- maven >= 3.6
- JDK >= 11

# Configuration

- `src/main/resources/application-local.properties`

# Installation

- `mvn clean package`

# Running

- `mvn spring-boot:run -Dspring.profiles.active=local`